setopt promptsubst;

# History settings.
HISTSIZE=10000;
HISTFILE="$HOME/.zsh_history";
SAVEHIST=10000;
setopt appendhistory;
setopt sharehistory;
setopt HIST_IGNORE_ALL_DUPS;

# Completion specific settings.
zstyle ':completion:*' completer _expand _complete _ignored _correct _approximate
zstyle ':completion:*' group-name ''
zstyle ':completion:*' insert-unambiguous true
zstyle ':completion:*' matcher-list 'm:{[:lower:]}={[:upper:]} r:|[._-]=* r:|=*' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]} r:|[._-]=* r:|=* l:|=*'
zstyle ':completion:*' max-errors 2
zstyle ':completion:*' original true
zstyle ':completion:*' verbose true

# Shell helpers.
pid_info() {
	ps --no-headers | awk '/zsh/ { count++ } { if ($1=='"$$"') { printf("%{[1;34m%}"); for (i = 1; i < count; i++) printf("^") } }';
}

bpwd() {
	echo "$VPWD" | awk '{ sub("'$HOMEROOT'", "~", $0); if ($0 != "~") print $0 }';
}

precmd() {
	# Never have TRAP as VPWD.
	[ "$(pwd)" = "$TRAP" ] && cd "$VPWD";

	VPWD="$(pwd)";

	# Completion overrides.
	compctl -/ -W "$VPWD" cd;
	compctl -f -W "$VPWD" '*';

	cd "$TRAP";
}

preexec() {
	cd "$VPWD";
}

help() {
	cat <<-HERE
	Welcome to the Shell.

	These commands might be useful:
	HERE

	for addr in $(echo $PATH | tr ':' '\n' | tail -n1); do
		ls "$addr";
	done
}

HOMEROOT="$HOME/root";
TRAP="/tmp/trap";
mkdir -p "$TRAP";

VPWD="$HOMEROOT";
PROMPT="$(pid_info)%{[0;36m%}%n%{[m%}>\$(bpwd) ";
bindkey -v;

cd "$HOMEROOT";
ls;
