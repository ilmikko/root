# TODO: Change this to ~/... when ready
ROOT_DIR="$HOME/root";

DIRTY_DIR="/tmp/$USER-dirty";
LOCAL_DIR="/tmp/$USER-local";

log() {
	echo "$@" 1>&2;
}

fail() {
	echo "$@" 1>&2;
	exit 1;
}

require_args() {
	name=$1;
	shift;
	while [ $# -gt 0 ]; do
		if [ -n "$1" ]; then
			case "$1" in
				--)
					shift;
					continue;
					;;
				--*)
					;;
				*)
					shift;
					continue;
					;;
			esac
		fi
		fail "$name needs $# more argument(s).";
	done
}

list_lower_dirs() {
	mount="$(mount | grep -m1 "\s$1\s")";
	log "Mount: $mount";
	echo "$mount" | sed -r 's/.*lowerdir=|,.*//g';
}

overlay_attach() {
	if mountpoint "$2" 1>/dev/null; then
		lowerdirs="$(list_lower_dirs "$2")";
		sudo umount "$2";
	fi

	if ! echo ":$lowerdirs:" | grep --silent ":$1:"; then
		if [ -n "$lowerdirs" ]; then
			lowerdirs="$1:$lowerdirs";
		else
			lowerdirs="$1";
		fi
	fi

	overlay_mount "$lowerdirs" "$2";
}

overlay_detach() {
	if ! mountpoint "$2" 1>/dev/null; then
		return;
	fi

	lowerdirs="$(list_lower_dirs "$2")";
	if ! echo ":$lowerdirs:" | grep --silent ":$1:"; then
		log "$1 looks already detached.";
		return;
	fi

	sudo umount "$2";
	# Remove directory from lower mounts
	lowerdirs="$(echo ":$lowerdirs:" | awk '{ sub(":'"$1"':", ":", $0); gsub("^:|:$", "", $0); print $0 }')";

	[ -z "$lowerdirs" ] && return;
	overlay_mount "$lowerdirs" "$2";
}

overlay_mount() {
	dirty_flush;

	log "LOWER DIRS: $1";
	sudo mount -t overlay overlay -o "lowerdir=$1,upperdir=$DIRTY_DIR/local,workdir=$DIRTY_DIR/work" "$2";
}

dirty_flush() {
	local_save;

	log "Flushing dirty directory...";
	rm -rf "$DIRTY_DIR";

	mkdir -p "$DIRTY_DIR";
	mkdir -p "$DIRTY_DIR/work" "$DIRTY_DIR/local";

	local_restore;
}

local_restore() {
	log "Restoring local changes...";
	cp -ruv "$LOCAL_DIR/." "$DIRTY_DIR/local";
}

local_save() {
	log "Saving local changes...";
	rm -rf "$LOCAL_DIR";
	mkdir -p "$LOCAL_DIR";

	cp -ruv "$DIRTY_DIR/local/." "$LOCAL_DIR";
}

main() {
	while [ "$#" -gt 0 ]; do
		case "$1" in
			attach)
				require_args "$1" "$2";
				shift;

				[ -d "$1" ] || fail "$1 is not a valid directory.";
				dir="$(realpath "$1")";

				[ -d "$ROOT_DIR" ] || mkdir -p "$ROOT_DIR";
				log "Attaching $dir to $ROOT_DIR";
				overlay_attach "$dir" "$ROOT_DIR";
				;;
			detach)
				require_args "$1" "$2";
				shift;

				[ -d "$1" ] || fail "$1 is not a valid directory.";
				dir="$(realpath "$1")";

				log "Detaching $dir from $ROOT_DIR";
				overlay_detach "$dir" "$ROOT_DIR";
				;;
			dirty) # List files that are only locally saved.
				find "$DIRTY_DIR/local/" | grep -v "^$DIRTY_DIR/local/$";
				;;
			list) # List which directories we have overlayed.
				list_lower_dirs "$ROOT_DIR" | sed -r 's/([^\]|^):/\1\n/g';
				;;
			*)
				fail "Unknown action: $1";
				;;
		esac
		shift;
	done
}
